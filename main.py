import asyncio

from wrapper import MyMMR

region = "euw"

headers = {
    "User-Agent": "iOS:com.mymmr.wrapper:v0.0.2"
}

mmrInstance = MyMMR(region, headers)



async def getPlayerStats():
    try:
        data= await mmrInstance.getNormalMMR("Test")
        print(data)
    except Exception as e:
        print(e)

loop = asyncio.get_event_loop()  

loop.run_until_complete(getPlayerStats())